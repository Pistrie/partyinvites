﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PartyInvites.Components;
using PartyInvites.Interfaces;
using PartyInvites.Models;

namespace PartyInvites.Controllers
{
    public class HomeController : Controller
    {
        private IRepository _repository;
        
        public IActionResult Index()
        {
            return View();
        }

        public HomeController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public ViewResult RsvpForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult RsvpForm(GuestResponse guestResponse)
        {
            if (ModelState.IsValid)
            {
                _repository.AddResponse(guestResponse);
                return View("Thanks", guestResponse);
            }

            return View();
        }

        public ViewResult ListResponses()
        {
            var responses = _repository.getResponses();
            return View(responses.Where(r => r.WillAttend == true));
        }

        public ViewResult ListResponse(int participant)
        {
            // als het null is dan is het een onjuiste participant
            return View(_repository.GetResponse(participant));
        }
    }
}