using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PartyInvites.Interfaces;
using PartyInvites.Models;

namespace PartyInvites.Components
{
    public class ResponsesSummary : ViewComponent
    {
        private IRepository _repository;

        public ResponsesSummary(IRepository repository)
        {
            _repository = repository;
        }

        public string Invoke()
        {
            return $"{_repository.getResponses().Count()} responses, "
                   + $"{_repository.getResponses().Count(r => r.WillAttend == true)} will attend the party";
        }
    }
}