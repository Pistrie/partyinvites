using System.Collections;
using System.Collections.Generic;
using PartyInvites.Models;

namespace PartyInvites.Interfaces
{
    public interface IRepository
    {
        // TODO: implementeer dit voor dependency injection
        public void AddResponse(GuestResponse response);
        public GuestResponse GetResponse(int i);
        public IEnumerable<GuestResponse> getResponses();
    }
}