using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PartyInvites.Interfaces;

// TODO: dependency injection
namespace PartyInvites.Models
{
    public class Repository : IRepository
    {
        private IEnumerable<GuestResponse> _responses = new List<GuestResponse>();

        // private readonly List<GuestResponse> responses = new();
        //
        // public IEnumerable<GuestResponse> Responses => responses;

        public void AddResponse(GuestResponse response)
        {
            // _responses.Add(response);
            _responses = _responses.Append(response);
        }

        public GuestResponse GetResponse(int i)
        {
            // return _responses.Count < i ? null : responses.ElementAt(i);
            if (_responses.Count() < i)
            {
                return null;
            }

            return _responses.ElementAt(i);
        }

        public IEnumerable<GuestResponse> getResponses()
        {
            return _responses;
        }
    }
}